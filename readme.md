# Read me please

The test was simple in requirement but I wanted to give some thoughts about the design file that was given.

The file seemed more like a styleguide rather than a mock up of a page so it seemed a bit confusing as to what I actually needed to build. So i've spent building what I think is a basic structure and other bits around it.

This has been design for mobile first and then there are two break points, 768 and 1280.

I've added google fonts as I didn't have any that were supplied with the PSD file. I could've used the system onces but these looked nicer and didn't require much effort to implement.

You may feel that I have missed out some bits but thats only because I didn't want to spend too much time on this, not saying that the quality doesn't matter. 

I used a couple of SVGs for high dpi devices.

# What could i have done but didn't have time

I could've done lots of things differently but time was limited.

* responsive images - though not hard to implement I have little expereience with photoshop. It took me ages to export a slice! I'm mainly a fireworks guy. It's superior to PS for web development IMO.
* sprites - I normally sprite up the svgs and serve them as a single request. Again time is my enemy.
* Javascript - not much javascript was needed here apart from the menu toggle and thats vanilla js.