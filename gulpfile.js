const postcss = require('gulp-postcss'),
	gulp = require('gulp'),
	autoprefixer = require('autoprefixer'),
	cssnano = require('gulp-cssnano'),
	nested = require('postcss-nested'),
	gulpif = require('gulp-if');

var env = process.env.NODE_ENV || 'development';

gulp.task('css', function () {
var plugins = [
	autoprefixer({
		browsers: ['last 1 version']
	}),
	nested
];
return gulp
	.src('./src/*.css')
	.pipe(postcss(plugins))
	.pipe(gulpif(env === 'production', cssnano()))
	.pipe(gulp.dest('./dest'));
});

gulp.task('watch', function() {
	gulp.watch('./src/*.css', ['css'])
});

gulp.task('default', ['css']);

// For production run `NODE_ENV=production gulp`